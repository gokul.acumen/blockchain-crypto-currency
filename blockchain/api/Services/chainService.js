let BlockChain = require("../Services/block")
var chainService = {
    start: async () => {
        let blockChain = new BlockChain();
        blockChain.addNewTransaction("gokul", "alex", 200);
        blockChain.addNewBlock(null);

        console.log("Chain : ", blockChain.chain);
    }
}
module.exports = chainService;