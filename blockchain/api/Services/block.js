let hash = require('object-hash');
//const Chain = require('../models/Chain');
const TARGET_HASH = hash(100);
var validator = require("../Services/validator")
class block {
    constructor() {
        this.chain = []
        this.curr_transactions = []
    }

    async addNewBlock() {
        try {
            this.chain = await Chain.find()
            this.prevHash = this.chain[this.chain.length - 1] ? this.chain[this.chain.length - 1].hash : "0";
            this.index = this.chain.length+1
            let block = {
                index: this.index,
                timestamp: Date.now(),
                transactions: this.curr_transactions,
                prevHash: this.prevHash
            }

            if (validator.proofOfWork() == TARGET_HASH) {
                block.hash = hash(block);
                sails.log.verbose("block", block)
                //Add it to the instance Save it on the DB Console Success
                await Chain.create(block)
                //Add to Chain
                this.chain.push(block);
                this.curr_transactions = [];
                return block;
            }
        } catch (error) {
            sails.log.error("CANNOT ADD THE BLOCK", error)
            throw error
        }

    }



    addNewTransaction(sender, recipient, amount) {
        this.curr_transactions.push({ sender, recipient, amount });
    }


    lastBock() {
        return this.chain.slice(-1)[0];
    }
}

module.exports = block;